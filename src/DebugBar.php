<?php

namespace Superatom\Middleware;

use DebugBar\Bridge\MonologCollector;
use DebugBar\DataCollector\ConfigCollector;
use DebugBar\DataCollector\MemoryCollector;
use DebugBar\DataCollector\PDO\PDOCollector;
use DebugBar\DataCollector\PDO\TraceablePDO;
use DebugBar\DataCollector\RequestDataCollector;
use DebugBar\DataCollector\TimeDataCollector;
use DebugBar\DebugBar as PhpDebugBar;
use DebugBar\Storage\FileStorage;
use Superatom\Application;
use Superatom\Middleware\DebugBar\AtomHttpDriver;
use Superatom\Middleware\DebugBar\AtomRouteCollector;
use Superatom\Middleware\DebugBar\AtomViewCollector;
use Superatom\Middleware\DebugBar\EnvironmentCollector;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DebugBar
{
    /**
     * @var Application
     */
    protected $app;

    /**
     * @var \PDO
     */
    protected $pdo = null;

    /**
     * @var PhpDebugBar
     */
    protected $phpdebugbar;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function setPdo(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * @param Request  $req
     * @param Response $res
     * @param callable $next
     *
     * @return Response
     */
    public function __invoke(Request $req, Response $res, callable $next)
    {
        // TODO: use custom data collector
        $timeDataCollector = new TimeDataCollector();
        $this->phpdebugbar = new PhpDebugBar();
        $this->phpdebugbar->addCollector($timeDataCollector);
        $this->phpdebugbar->addCollector(new RequestDataCollector());
        $this->phpdebugbar->addCollector(new MemoryCollector());
        $this->phpdebugbar->addCollector(new MonologCollector($this->app->logger));
        $this->phpdebugbar->addCollector(new EnvironmentCollector($this->app->environment));
        $this->phpdebugbar->addCollector(new ConfigCollector($this->app->config->all()));
        $this->phpdebugbar->addCollector(new AtomRouteCollector($this->app->router));
        if ($this->pdo) {
            $this->phpdebugbar->addCollector(new PDOCollector(new TraceablePDO($this->pdo), $timeDataCollector));
        }
        $dir = $this->app->config->get('debugbar.storage', storage_path('debugbar'));
        $storage = new FileStorage($dir);
        $this->phpdebugbar->setStorage($storage);

        $this->app->debugbar = $this->phpdebugbar;
        $this->setAssetsRoute();

        /** @var Response $newResponse */
        $newResponse = $next($req, $res);

        // collect rendered view data
        $this->phpdebugbar->addCollector(new AtomViewCollector($this->app->view->getDataAll()));

        if ($this->isAssetsRoute()) {
            return $newResponse;
        }

        $session = isset($this->app['session']) ? $this->app['session'] : null;
        $httpDriver = new AtomHttpDriver($newResponse, $session);
        $this->phpdebugbar->setHttpDriver($httpDriver);

        if ($req->isXmlHttpRequest()) {
            if ($this->phpdebugbar->getStorage()) {
                $this->phpdebugbar->sendDataInHeaders($useOpenHandler = true);

                return $newResponse;
            }
        }

        if (!$this->isModifiable($newResponse)) {
            return $newResponse;
        }

        $html = $this->modifyResponse($newResponse->getContent());
        $newResponse->setContent($html);

        return $newResponse;
    }

    public function isModifiable(Response $response)
    {
        if ($response->isRedirect()) {
            if ($this->phpdebugbar->getHttpDriver()->isSessionStarted()) {
                $this->phpdebugbar->stackData();
            }

            return false;
        }

        $content_type = $response->headers->get('Content-Type');
        if (stripos($content_type, 'html') === false) {
            return false;
        }

        return true;
    }

    public function modifyResponse($html)
    {
        $debug_html = $this->getDebugHtml();
        $pos = mb_strripos($html, '</body>');
        if ($pos === false) {
            $html .= $debug_html;
        } else {
            $html = mb_substr($html, 0, $pos).$debug_html.mb_substr($html, $pos);
        }

        return $html;
    }

    public function getDebugHtml()
    {
        $renderer = $this->phpdebugbar->getJavascriptRenderer();
        if ($this->phpdebugbar->getStorage()) {
            $renderer->setOpenHandlerUrl($this->app->router->pathFor('debugbar.openhandler'));
        }

        $html = $this->getAssetsHtml();
        if ($renderer->isJqueryNoConflictEnabled()) {
            $html .= "\n".'<script type="text/javascript">jQuery.noConflict(true);</script>';
        }

        return $html."\n".$renderer->render();
    }

    public function getAssetsHtml()
    {
        return <<<HTML
<link rel="stylesheet" type="text/css" href="/_debugbar/resources/dump.css">
<script type="text/javascript" src="/_debugbar/resources/dump.js"></script>
HTML;
    }

    protected function setAssetsRoute()
    {
        $controller = '\Superatom\Middleware\DebugBar\DebugBarController';
        $this->app->router->get('/_debugbar/fonts/{file}', "{$controller}@getFont")->setName('debugbar.fonts');
        $this->app->router->get('/_debugbar/resources/{file}', "{$controller}@getResource")->setName('debugbar.resources');
        $this->app->router->get('/_debugbar/openhandler', "{$controller}@getOpenHandler")->setName('debugbar.openhandler');
    }

    protected function isAssetsRoute()
    {
        $route = $this->app->router->getCurrentRoute();
        if ($route) {
            return (explode('.', $route->getName())[0] === 'debugbar');
        }

        return false;
    }
}
