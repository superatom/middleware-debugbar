<?php

namespace Superatom\Middleware\DebugBar;

use DebugBar\DataCollector\DataCollector;
use DebugBar\DataCollector\Renderable;
use Superatom\Routing\Route;
use Superatom\Routing\Router;

class AtomRouteCollector extends DataCollector implements Renderable
{
    /**
     * @var Router
     */
    protected $router;

    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    /**
     * Called by the DebugBar when data needs to be collected.
     *
     * @return array Collected data
     */
    public function collect()
    {
        $route = $this->router->getCurrentRoute();

        return $this->getRouteInfo($route);
    }

    /**
     * Returns the unique name of the collector.
     *
     * @return string
     */
    public function getName()
    {
        return 'route';
    }

    /**
     * Returns a hash where keys are control names and their values
     * an array of options as defined in {@see DebugBar\JavascriptRenderer::addControl()}.
     *
     * @return array
     */
    public function getWidgets()
    {
        return [
            'route' => [
                'icon' => 'share',
                'widget' => 'PhpDebugBar.Widgets.VariableListWidget',
                'map' => 'route',
                'default' => '{}',
            ],
            'currentroute' => [
                'icon' => 'share',
                'tooltip' => 'Route',
                'map' => 'route.uri',
                'default' => '',
            ],
        ];
    }

    protected function getRouteInfo(Route $route = null)
    {
        if (!$route) {
            return [];
        }

        $uri = head($route->getMethods()).' '.$route->getPattern();
        $name = $route->getName();
        $handler = $route->getHandler();
        if ($handler instanceof \Closure) {
            $handler = 'Closure';
        }

        return compact('uri', 'name', 'handler');
    }
}
