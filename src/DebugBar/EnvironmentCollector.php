<?php

namespace Superatom\Middleware\DebugBar;

use DebugBar\DataCollector\DataCollector;
use DebugBar\DataCollector\Renderable;
use Superatom\Application;

class EnvironmentCollector extends DataCollector implements Renderable
{
    /**
     * Application environment.
     *
     * @var string
     */
    protected $env;

    public function __construct($env)
    {
        $this->env = $env;
    }

    /**
     * Called by the DebugBar when data needs to be collected.
     *
     * @return array Collected data
     */
    public function collect()
    {
        return [
            'php' => PHP_VERSION,
            'version' => Application::VERSION,
            'environment' => $this->env,
        ];
    }

    /**
     * Returns the unique name of the collector.
     *
     * @return string
     */
    public function getName()
    {
        return 'atom';
    }

    /**
     * Returns a hash where keys are control names and their values
     * an array of options as defined in {@see DebugBar\JavascriptRenderer::addControl()}.
     *
     * @return array
     */
    public function getWidgets()
    {
        return [
            'php' => [
                'icon' => 'info',
                'tooltip' => 'PHP version',
                'map' => 'atom.php',
                'default' => '',
            ],
            'version' => [
                'icon' => 'github',
                'tooltip' => 'Atom version',
                'map' => 'atom.version',
                'default' => '',
            ],
            'environment' => [
                'icon' => 'desktop',
                'tooltip' => 'Environment',
                'map' => 'atom.environment',
                'default' => '',
            ],
        ];
    }
}
