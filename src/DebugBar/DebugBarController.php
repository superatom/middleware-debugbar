<?php

namespace Superatom\Middleware\DebugBar;

use DebugBar\JavascriptRenderer;
use DebugBar\OpenHandler;
use Superatom\Routing\Controller;

class DebugBarController extends Controller
{
    public function getFont($file)
    {
        $files = explode('?', $file);
        $file = reset($files);
        $path = $this->renderer()->getBasePath().'/vendor/font-awesome/fonts/'.$file;
        if (!is_file($path)) {
            $this->response->setStatusCode(404);
            $this->response->setContent("$file not found.");

            return $this->response;
        }

        $this->response->setContent(file_get_contents($path));
        $this->response->headers->set('Content-Type', (new \finfo(FILEINFO_MIME))->file($path));

        return $this->response;
    }

    public function getResource($file)
    {
        $files = explode('.', $file);
        $ext = end($files);
        if ($ext === 'css') {
            $css = $this->dumpAssets(function () { $this->renderer()->dumpCssAssets(); });
            $this->response->setContent($css);
            $this->response->headers->set('Content-Type', 'text/css');
        } elseif ($ext === 'js') {
            $js = $this->dumpAssets(function () { $this->renderer()->dumpJsAssets(); });
            $this->response->setContent($js);
            $this->response->headers->set('Content-Type', 'text/javascript');
        } else {
            $path = $this->renderer()->getBasePath().'/'.$file;
            $this->response->setContent(file_get_contents($path));
            $this->response->headers->set('Content-Type', 'image/png');
        }

        return $this->response;
    }

    public function getOpenHandler()
    {
        $openHandler = new OpenHandler($this->app['debugbar']);
        $data = $openHandler->handle($request = null, $echo = false, $sendHeader = false);
        $this->response->setContent($data);
        $this->response->headers->set('Content-Type', 'application/json');

        return $this->response;
    }

    /**
     * @return JavascriptRenderer
     */
    protected function renderer()
    {
        return $this->app['debugbar']->getJavascriptRenderer();
    }

    /**
     * @param callable $callable
     *
     * @return string
     */
    protected function dumpAssets($callable)
    {
        ob_start();
        $callable();
        $output = ob_get_clean();

        return $output;
    }
}
