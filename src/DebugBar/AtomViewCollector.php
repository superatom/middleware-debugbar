<?php

namespace Superatom\Middleware\DebugBar;

use DebugBar\DataCollector\ConfigCollector;

class AtomViewCollector extends ConfigCollector
{
    public function __construct(array $data = [])
    {
        parent::__construct($data, 'view');
    }

    public function getName()
    {
        return 'view';
    }
}
