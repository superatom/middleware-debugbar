<?php

namespace Superatom\Middleware\DebugBar;

use DebugBar\HttpDriverInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class AtomHttpDriver implements HttpDriverInterface
{
    /**
     * @var Response
     */
    protected $response;

    /**
     * @var SessionInterface
     */
    protected $session = null;

    public function __construct(Response $response, SessionInterface $session = null)
    {
        $this->response = $response;
        $this->session = $session;
    }

    /**
     * Sets HTTP headers.
     *
     * @param array $headers
     */
    public function setHeaders(array $headers)
    {
        $this->response->headers->add($headers);
    }

    /**
     * Checks if the session is started.
     *
     * @return bool
     */
    public function isSessionStarted()
    {
        if ($this->session) {
            return $this->session->isStarted();
        }

        return false;
    }

    /**
     * Sets a value in the session.
     *
     * @param string $name
     * @param mixed  $value
     */
    public function setSessionValue($name, $value)
    {
        $this->session->set($name, $value);
    }

    /**
     * Checks if a value is in the session.
     *
     * @param string $name
     *
     * @return bool
     */
    public function hasSessionValue($name)
    {
        return $this->session->has($name);
    }

    /**
     * Returns a value from the session.
     *
     * @param string $name
     *
     * @return mixed
     */
    public function getSessionValue($name)
    {
        return $this->session->get($name);
    }

    /**
     * Deletes a value from the session.
     *
     * @param string $name
     */
    public function deleteSessionValue($name)
    {
        $this->session->remove($name);
    }
}
